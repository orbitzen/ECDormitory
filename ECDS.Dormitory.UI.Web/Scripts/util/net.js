﻿var $net = function(){
    return {
        ajax: function (httpMethod, url, request, cbSuccess, cbError, isAsync) {

            var options = {
                type: httpMethod,
                url: url,
                data: request,
                success: cbSuccess,
                error: cbError
            };

            if (!isAsync) {
                options.async = isAsync;
            }

            $.ajax(options);
        }
    }
}();